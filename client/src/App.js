import React from 'react';
import Product from './components/Product';
import Homepage from './components/Homepage'
import './App.css';
import { Provider } from 'react-redux'
import store from './store'

function App() {
  return (
    <Provider store={store}>
    <div className="App">
      {/* <Product /> */}
      <Homepage />
    </div>
    </Provider>
  );
}

export default App;
