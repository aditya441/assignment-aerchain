import React, { Component } from 'react'

export default class Product extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             text: '',
             input: '',
             URL: "http://localhost:3000/product/dsd"
        }
    }
    
    componentDidMount(){
        fetch('/product/dsd')
        .then(async res => { 
            // const data = JSON.parse(res); 
            // console.log(res) 
            const data = await res.json();
            console.log(data);
        })
        // .then(res => {
        //         this.setState({ text: res })
        //     })
    }

    handleChange = e => {
        this.setState({ input: e.target.value })
    }

    handleSubmit = e => {
        if(document.URL === "http://localhost:3000/product/dsd") {
            fetch("/product/dsd", {
            method: "POST",
            headers: {
                "Content-type": "application/json"
                },
                body: JSON.stringify(this.state.input)
            })      
            .then(res => res.text())
            .then(res => alert(res));
        } 
    }

    render() {
        return (
            document.URL === this.state.URL ? <div>
                
                Hi Product page {this.state.text}
                <input type="text" placeholder="Enter here..." onChange={this.handleChange} name="inputBox" value={this.state.input} />
                <button onClick={this.handleSubmit}>{`submit`}</button>
                </div> : <div>Page Not Found</div>
        )
    }
}
