import React, { Component } from 'react'

export class Homepage extends Component {
    addCategory = () => {
        console.log('pressed')
    }
    render() {
        return (
            <div>
                <button type="button" className="btn btn-dark btn-lg" onClick = {this.addCategory}>Dark</button>
            </div>
        )
    }
}

export default Homepage
